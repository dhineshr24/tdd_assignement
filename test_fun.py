from pythonProject.demoday5.fun import StudentDB
import pytest

db = None


# setup module to connect the database only once
def setup_module(module):
    global db
    db = StudentDB()
    db.connect('data.json')


# teardown module to close the database connection when both test will run
def teardown_module(module):
    db.close()


# test to check the scott data
def test_scott_data():
    scott_data = db.get_data('Scott')
    assert scott_data['id'] == 1
    assert scott_data['name'] == 'Scott'
    assert scott_data['result'] == "pass"


# test to check the mark data
def test_mark_data():
    scott_data = db.get_data('Mark')
    assert scott_data['id'] == 1
    assert scott_data['name'] == 'Mark'
    assert scott_data['result'] == "fail"
